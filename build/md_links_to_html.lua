function RewriteLink ( link )
  -- don't reformat already existing links
  if string.find(link.target, "^https?://") or string.find(link.target, "%.html?$") then
    return link
  end

  link.target=string.gsub(link.target, "%..+$", ".html")
  return link
end


return {{ Link = RewriteLink }}
