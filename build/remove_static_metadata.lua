local vars = {
  filedir = pandoc.Str(""),
  address = pandoc.Str("https://example.com")
}

function GetVars (meta)
  for k, v in pairs(meta) do
    if k == "bibliography" or k == "csl" then
      meta[k] = nil
    end
  end
  return meta
end

return {{ Meta = GetVars }}
