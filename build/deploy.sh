#!/usr/bin/env sh

TARGET_DIR="${TARGET_DIR:-public}"
SOURCE_DIR="${SOURCE_DIR:-.}"
LIB_DIR="${LIB_DIR:-build}"
BIBLIOGRAPHY="${BIBLIOGRAPHY:-""}"
CSL="${CSL:-""}"
VERBOSE="${VERBOSE:-0}"
DRYRUN="${DRYRUN:-0}"
TMP_DIR="${TMP_DIR:-/tmp}"
STYLE="${STYLE:-""}"

is_dryrun() {
  if [ "$DRYRUN" -eq 1 ]; then
    true
  else
    false
  fi
}

is_verbose() {
  if [ "$VERBOSE" -eq 2 ] && [ "$1" -eq 2 ]; then
    true
  elif [ "$VERBOSE" -ge 1 ] && [ "$1" -ne 2 ]; then
    true
  else
    false
  fi
}

msg() {
  lvl="$1"
  shift

  if is_verbose "$lvl"; then
    echo "$@"
  fi
}

pandoc_compilation() {
  if is_dryrun; then
    return 0
  fi

  bname="$1"
  shift
  torun="pandoc -f markdown+smart -t html5 --lua-filter ${LIB_DIR}/md_links_to_html.lua --standalone --metadata title=\"${bname}\" -V lang=en"

  [ -n "$STYLE" ] && torun="${torun} --css=$STYLE -V highlighting-css= --mathjax"
  [ -n "$BIBLIOGRAPHY" ] && torun="${torun} --metadata bibliography=$BIBLIOGRAPHY --filter pandoc-citeproc"
  [ -n "$CSL" ] && torun="${torun} --metadata csl=$CSL"

  torun="${torun} \"${SOURCE_DIR}/${bname}.md\" >\"${TARGET_DIR}/${bname}.html\""

  eval "$torun"

}

ensure_target_dir() {
  if is_dryrun; then
    return 0
  fi

  mkdir -p "$1"
}

get_style() {
  # if it is a linked bibliography, pull it down
  case "$1" in *https://* | *http://*)
    mkdir -p "${TARGET_DIR}/css"
    curl "$1" --silent --output "${TARGET_DIR}"/css/style.css
    STYLE="css/style.css"
    ;;
  esac
}

get_csl() {
  # if it is a linked bibliography, pull it down
  case "$1" in *https://* | *http://*)
    curl "$1" --silent --output "${TMP_DIR}"/csl.csl
    CSL="${TMP_DIR}/csl.csl"
    ;;
  esac
}

get_bibliography() {
  # if it is a linked bibliography, pull it down
  case "$1" in *https://* | *http://*)
    curl "$1" --silent --output "${TMP_DIR}"/bibli.bib
    BIBLIOGRAPHY="${TMP_DIR}/bibli.bib"
    ;;
  esac
}

compile_file() {
  bname="$(basename "$1" | sed -e 's/.md$//')"

  msg 2 "${bname}.md > $TARGET_DIR/${bname}.html"
  pandoc_compilation "$bname"
}

cleanup() {
  [ -f "${TMP_DIR}/csl.csl" ] && rm "${TMP_DIR}"/csl.csl
  [ -f "${TMP_DIR}/bibli.bib" ] && rm "${TMP_DIR}"/bibli.bib
  [ -f "${TMP_DIR}/tmp" ] && rm "${TMP_DIR}"/tmp
}

main() {
  get_bibliography "$BIBLIOGRAPHY"
  get_csl "$CSL"
  get_style "$STYLE"
  ensure_target_dir "$TARGET_DIR"

  find "$SOURCE_DIR" ! -name "$(printf "*\n*")" -name '*.md' >"${TMP_DIR}"/tmp
  # when killed rm tmp file; AND invoke usr signal
  trap cleanup EXIT

  numfiles="$(wc -l <"${TMP_DIR}"/tmp)"
  count=0
  while IFS= read -r file; do
    count=$((count + 1))
    msg 1 "$count/$numfiles"

    compile_file "$file"
  done <"${TMP_DIR}"/tmp
}

while getopts "vnb:c:" opt; do
  case "$opt" in
  v) VERBOSE=$((VERBOSE + 1)) ;;
  n) DRYRUN=1 ;;
  b) BIBLIOGRAPHY="$OPTARG" ;;
  c) STYLE="$OPTARG" ;;
  *)
    echo "Usage: deploy.sh [-v: verbose display] [-n: dry-run, do not create compiled files] [-b path/to/biliography_file ]"
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

[ "${1:-}" = "--" ] && shift

main "$@"
