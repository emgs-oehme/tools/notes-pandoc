FROM pandoc/core
MAINTAINER Marty Oehme <marty.oehme@gmail.com>

RUN apk add --no-cache curl

WORKDIR /data
COPY build /build

ENV LIB_DIR /build

ENTRYPOINT ["/build/deploy.sh"]
